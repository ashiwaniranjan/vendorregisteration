import React, { createContext, useState } from "react";
export const UserContext = createContext([{}, () => {}]);

export default props => {
  const [state, setState] = useState({
    user: {
      businessname: "",
      email: "",
      password: "",
      confirmPassword: "",
      location: "",
      vechicletype: "",
      vechiclemodel: "",
      vechicleinventory: "",
      bio: "",
      website: "",
      acceptTerms: false,
      newsletter: false,
      poiid: 1234
    },
    errors: {}
  });
  return (
    <UserContext.Provider value={[state, setState]}>
      {props.children}
    </UserContext.Provider>
  );
};
